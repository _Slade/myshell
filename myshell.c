/*
=pod

=head1 myshell

=head1 AUTHOR

Christopher Constantino <csc34@pitt.edu>

=head1 SUMMARY

A simple Unix shell. Supports all features given by the project specification.

=cut
*/
#include <assert.h>
#include <errno.h>
#include <pwd.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#define STREQ(a, b) (strcmp((a), (b)) == 0)
#define INPUT_BUFFER_SIZE 2048
#define ARGLIST_SIZE 32

char const *g_delims = " \t()|&;";
char const *g_home_path;
bool g_no_home;

/*
Notes:
    foo >bar
    foo <bar
    foo >>bar
are all permitted.
Comments begin with # and end at the end of the line.
I'm not planning on supporting continuations (\) yet.
*/

int display_prompt(void)
{
    return fputs("$ ", stdout);
}

/* Ideally this would be as simple as C<line[strcspn(line, "#\n")] = '\0'>, but
it needs to be able to support backslash escapes */
void process_metacharacters(char *line)
{
    size_t const length = strlen(line);
    for (size_t i = 0; i < length; ++i)
    {
        if (line[i] == '\n' || line[i] == '#')
        {
            if (i != 0 && line[i - 1] == '\\')
            {
                line[i - 1] = ' ';
                return;
            }
            else
            {
                line[i] = '\0';
                return;
            }
        }
    }
}

/*
=head2 bool builtin(char const *token)

If I<token> specifies a built-in command: execute the command and return true.
Otherwise, return false.

If I<token> is "exit", this function does not return.

Individual commands are as follows:

=head3 exit

Exits the current interactive session.

=head3 cd

Changes the current working directory to the given path. If no path is
specified, attempts to find the current user's home directory. Otherwise, an
error occurs.

=cut
*/
bool builtin(char const *token)
{
    if (token == NULL)
    {
        return false;
    }
    if (STREQ(token, "exit"))
    {
        exit(0);
    }
    else if (STREQ(token, "cd"))
    {
        char const *path = strtok(NULL, g_delims);
        if (path == NULL)
        {
            if (g_no_home)
            {
                fputs("No home path.", stderr);
                return true;
            }
            path = g_home_path;
        }
        if (chdir(path))
        {
            perror("Could not execute built-in command cd");
            #ifdef DEBUG
            fprintf(stderr, "The offending path is: %s\n", path);
            #endif
        }
        return true;
    }
    return false;
}

void initialize_home(void)
{
    g_home_path = getenv("HOME");
    if (g_home_path == NULL)
    {
        g_home_path = (getpwuid(getuid()))->pw_dir;
        if (g_home_path == NULL)
        {
            perror("Home directory could not be found");
            g_no_home = true;
        }
    }
}

int main(int argc, char *argv[])
{
    char buf[INPUT_BUFFER_SIZE];
    initialize_home();
    for (;;)
    {
        display_prompt();

        if (fgets(buf, sizeof(buf), stdin) == NULL)
        {
            exit(0);
        }

        process_metacharacters(buf);

        int child_argc = 0;
        char *child_argv[ARGLIST_SIZE] = { NULL };
        char *token = strtok(buf, g_delims);

        /* Handle built-ins */
        if (builtin(token))
            continue;
        else
            child_argv[child_argc++] = token;

        pid_t child;
        int status = 0;
        if ((child = fork()) == 0) /* Child code */
        {
            while (token = strtok(NULL, g_delims))
            {
                if (child_argc > ARGLIST_SIZE - 2) /* Last one must be NULL */
                {
                    fputs("Error: Too many arguments.\n", stderr);
                    exit(0);
                }
                else if (token[0] == '<' || token[0] == '>')
                {
                    char *path;       /* File to redirect stream to/from */
                    FILE *stream;     /* The stream to be redirected */
                    char const *mode; /* Open mode */

                    #ifdef DEBUG
                    mode   = NULL;
                    stream = NULL;
                    path   = NULL;
                    #endif

                    path = token + strspn(token, "<>");
                    if (path[0] == '\0') /* Next token is our path */
                    {
                        path = strtok(NULL, g_delims);
                        if (path == NULL)
                        {
                            fputs(
                                "Error: No path given to redirection.\n",
                                stderr
                            );
                            exit(0);
                        }
                    }
                    #ifdef DEBUG
                    else
                    {
                        fprintf(
                            stderr,
                            "Debug: \npath  = %s\ntoken = %s\n",
                            path,
                            token
                        );
                    }
                    #endif

                    if (token[0] == '<')
                    {
                        mode = "r";
                        stream = stdin;

                    }
                    else if (token[0] == '>' && token[1] == '>')
                    {
                        mode = "a";
                        stream = stdout;
                    }
                    else /* token[0] == '>' && token[1] != '>' */
                    {
                        mode = "w";
                        stream = stdout;
                    }

                    assert(path != NULL);
                    assert(mode != NULL);
                    assert(stream != NULL);
                    #ifdef DEBUG
                    fprintf(
                        stderr,
                        "Redirecting %s %s\n",
                        stream == stdin ? "stdin from" : "stdout to",
                        path
                    );
                    #endif

                    if (freopen(path, mode, stream) == NULL)
                    {
                        perror("Error");
                        exit(0);
                    }
                }
                else
                {
                    child_argv[child_argc] = token;
                    assert(token != NULL);
                    child_argc += 1;
                }
            }

            if (child_argv[0] == NULL)
                exit(0);
            #if 0
            fputs("{ ", stderr);
            for (size_t i = 0; child_argv[i] != NULL; ++i)
            {
                fprintf(stderr, "\"%s\", ", child_argv[0]);
            }
            fputs(" }\n", stderr);
            #endif

            if (execvp(child_argv[0], child_argv))
            {
                perror("Could not exec");
                exit(EXIT_FAILURE);
            }
        }
        else /* Parent code */
        {
            waitpid(child, &status, 0);
            if (status)
            {
                fprintf(stderr, "Child exited with status %d.\n", status);
            }
        }
    }
    return EXIT_SUCCESS;
}

