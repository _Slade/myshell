CC = gcc
CFLAGS = -Wall -std=gnu99 -Wno-parentheses
RM = rm -f

debug : CFLAGS += -DDEBUG -g
debug : myshell

myshell : myshell.c

clean :
	$(RM) myshell

